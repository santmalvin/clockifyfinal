package com.example.clockifyfinal.service.auth;

import com.example.clockifyfinal.model.ActivityDetailModel;
import com.example.clockifyfinal.model.ActivityListModel;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ActivityService {
    @GET("timers")
    Call<ActivityListModel> getActivityList();

    @FormUrlEncoded
    @POST("timers")
    Call<Void> createActivtiyList(@Field("start_time") String startTime,
                                    @Field("end_time") String endTime,
                                    @Field("start_date") String startDate,
                                    @Field("end_date") String endDate,
                                    @Field("duration") String duration,
                                    @Field("activity") String activity,
                                    @Field("location") String location);

    @GET("timers/{id}")
    Call<ActivityDetailModel> getActivityDetail(@Path("id") String id);

    @DELETE("timers/{id}")
    Call<Void> deleteActivity(@Path("id") String id);

    @FormUrlEncoded
    @PUT("timers/{id}")
    Call<Void> updateActivity(@Path("id") String id,
                              @Field("activity") String activity);
}
