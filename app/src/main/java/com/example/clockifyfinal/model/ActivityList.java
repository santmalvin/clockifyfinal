package com.example.clockifyfinal.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ActivityList implements Parcelable {

    public String date;

    protected ActivityList(Parcel in) {
        date = in.readString();
    }

    public static final Creator<ActivityList> CREATOR = new Creator<ActivityList>() {
        @Override
        public ActivityList createFromParcel(Parcel in) {
            return new ActivityList(in);
        }

        @Override
        public ActivityList[] newArray(int size) {
            return new ActivityList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
    }
}
