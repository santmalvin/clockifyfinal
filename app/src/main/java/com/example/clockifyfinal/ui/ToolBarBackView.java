package com.example.clockifyfinal.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.clockifyfinal.R;

public class ToolBarBackView extends ConstraintLayout {
    private ImageView mBackImageView;
    private TextView mLabelTextView;
    public ToolBarBackView(Context context) {
        super(context);
        init(context, null, 0);
    }
    public ToolBarBackView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public ToolBarBackView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }
    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        inflate(context, R.layout.toolbar_back, this);
        mBackImageView = findViewById(R.id.iv_wellness_back);
        mLabelTextView = findViewById(R.id.tv_wellness_toolbar_title);
        if (attrs != null) {
            TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ToolBarBackView);
            try {
                mLabelTextView.setText(attributes.getString(R.styleable.ToolBarBackView_text));
            }
            finally {
                attributes.recycle();
            }
        }
    }
    public void setToolbarText(String text){
        if(text != null)
            mLabelTextView.setText(text);
    }
    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        setOnBackClickListener(l);
    }
    public void setOnBackClickListener(OnClickListener clickListener) {
        if (clickListener != null)
            mBackImageView.setOnClickListener(clickListener);
    }
}
