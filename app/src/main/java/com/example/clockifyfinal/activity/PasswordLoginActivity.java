package com.example.clockifyfinal.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.service.AuthResponse;
import com.example.clockifyfinal.service.ClockifyService;
import com.example.clockifyfinal.service.auth.AuthService;
import com.example.clockifyfinal.ui.ToolBarBackView;
import com.example.clockifyfinal.util.UserDefaults;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordLoginActivity extends AppCompatActivity {


    private ToolBarBackView backButton;
    private TextInputEditText password;
    private Button okButton;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_login);

        initData();
        initUI();
        initEvent();
    }

    private void initData(){
        email = getIntent().getStringExtra("email_string");
    }

    private void initUI(){
        backButton = findViewById(R.id.back_Button);
        password = findViewById(R.id.text_edit_password);
        okButton = findViewById(R.id.okButton);
    }

    private void initEvent(){
        backButton.setOnBackClickListener(v ->{
            Intent intent = new Intent(PasswordLoginActivity.this,LoginActivity.class);
            startActivity(intent);
        });

        okButton.setOnClickListener(v ->{
            login();
        });
    }

    private void login(){
        AuthService service = ClockifyService.create(AuthService.class);
        service.loginUser(email,password.getText().toString()).enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                if (response.isSuccessful() && response.body().token != null){
                    String token = response.body().token;
                    UserDefaults.getInstance().setString(UserDefaults.TOKEN_KEY, "Bearer " +token);
                    Intent intent = new Intent(PasswordLoginActivity.this,HomeActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(PasswordLoginActivity.this,"Password Salah", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Toast.makeText(PasswordLoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}