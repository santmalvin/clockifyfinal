package com.example.clockifyfinal.service.auth;

import com.example.clockifyfinal.service.AuthResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AuthService {
    @FormUrlEncoded
    @POST("users/sessions")
    Call<AuthResponse> loginUser(@Field("email") String email,
                                 @Field("password") String password);

    @FormUrlEncoded
    @POST("users/")
    Call<Void> signUpUser(@Field("email") String email,
                         @Field("name") String name,
                         @Field("password") String password);
}
