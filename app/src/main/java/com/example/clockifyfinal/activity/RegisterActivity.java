package com.example.clockifyfinal.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.service.ClockifyService;
import com.example.clockifyfinal.service.auth.AuthService;
import com.example.clockifyfinal.ui.ToolBarBackView;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private ToolBarBackView backButton;
    private Button registerButton;
    private Dialog popupDialog;
    private CardView cardView;
    private TextInputEditText email;
    private TextInputEditText password;
    private TextInputEditText confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initUi();
        initEvent();
    }

    private void initUi(){
        backButton = findViewById(R.id.back_Button);
        registerButton = findViewById(R.id.button_create_account);
        email = findViewById(R.id.text_edit_email);
        password = findViewById(R.id.text_edit_password);
        confirmPassword = findViewById(R.id.text_edit_confirm_password);
    }

    private void initEvent(){
        backButton.setOnBackClickListener(v ->{
            Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
            startActivity(intent);
        });

        registerButton.setOnClickListener(v -> {
            final String Email = email.getText().toString();
            final String Password = password.getText().toString();
            final String ConfirmPassword = confirmPassword.getText().toString();

            if (Email.length() == 0) {
                email.requestFocus();
                email.setError("FIELD CANNOT BE EMPTY");
            } else if (Password.length() == 0) {
                password.requestFocus();
                password.setError("FIELD CANNOT BE EMPTY");
            } else if (ConfirmPassword.length() == 0) {
                confirmPassword.requestFocus();
                confirmPassword.setError("FIELD CANNOT BE EMPTY");
            }else{
                signUp();
            }
        });
    }

    private void initSuccessDialog() {
        popupDialog = new AppCompatDialog(this);
        popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popupDialog.setContentView(R.layout.pop_up);
        Window window = popupDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        popupDialog.setCanceledOnTouchOutside(false);

        cardView = popupDialog.findViewById(R.id.cardView);

        cardView.setOnClickListener(m -> {
            if (popupDialog.isShowing()) {
                popupDialog.dismiss();
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        popupDialog.show();
    }

    private void signUp(){
        AuthService service = ClockifyService.create(AuthService.class);
        service.signUpUser(email.getText().toString(),"Bambang",password.getText().toString()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()){
                    initSuccessDialog();
                } else {
                    Toast.makeText(RegisterActivity.this,"Password Salah", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}