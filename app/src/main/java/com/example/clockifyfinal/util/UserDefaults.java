package com.example.clockifyfinal.util;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.clockifyfinal.BaseApplication;

public class UserDefaults {
    private static UserDefaults ourInstance;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    //main user preference
    public static final String TOKEN_KEY = "token";
    public static UserDefaults getInstance() {
        if (ourInstance == null) {
            ourInstance = new UserDefaults();
        }
        return ourInstance;
    }

    private UserDefaults() {
        pref = PreferenceManager.getDefaultSharedPreferences(BaseApplication.getAppContext());
        editor = pref.edit();
    }
    public void setInt(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }
    public void setLong(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }
    public void setString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }
    public void setBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }
    public void remove(String key) {
        editor.remove(key);
        editor.apply();
    }
    public String getString(String key) {
        return pref.getString(key, null);
    }
    public boolean getBool(String key) {
        return pref.getBoolean(key, false);
    }
    public boolean getBoolDefTrue(String key) {
        return pref.getBoolean(key, true);
    }
    public int getInt(String key, int defaultValue) {
        return pref.getInt(key, defaultValue);
    }
    public long getLong(String key, int defaultValue) {
        return pref.getLong(key, defaultValue);
    }
    public void clear() {
        pref.edit().clear().apply();
    }
}
