package com.example.clockifyfinal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.fragment.ActivityListFragment;
import com.example.clockifyfinal.fragment.TimerFragment;
import com.example.clockifyfinal.model.ActivityListData;
import com.example.clockifyfinal.model.ActivityListModel;
import com.example.clockifyfinal.service.ClockifyService;
import com.example.clockifyfinal.service.auth.ActivityService;
import com.google.android.material.tabs.TabLayout;

import org.w3c.dom.Text;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DemoCollectionPagerAdapter adapter;
    private FragmentManager manager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initLayout();
    }

    private void initLayout() {
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.pager);

        adapter = new DemoCollectionPagerAdapter(manager);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {
        public DemoCollectionPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "TIMER";
            } else {
                return "ACTIVITY";
            }
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment timerFragment = new TimerFragment();
            Bundle argsTimerFragment = new Bundle();
            timerFragment.setArguments(argsTimerFragment);


            Fragment activityListFragment = new ActivityListFragment();
            Bundle argsActivityFragment = new Bundle();
            activityListFragment.setArguments(argsActivityFragment);

            if (position == 0) {
                return timerFragment;
            } else {
                return activityListFragment;
            }
        }
    }

}