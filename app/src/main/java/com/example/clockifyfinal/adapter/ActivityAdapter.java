package com.example.clockifyfinal.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.activity.ActivityDetailActivity;
import com.example.clockifyfinal.activity.HomeActivity;
import com.example.clockifyfinal.activity.PasswordLoginActivity;
import com.example.clockifyfinal.model.ActivityDetailModel;
import com.example.clockifyfinal.model.ActivityList;
import com.example.clockifyfinal.model.ActivityListData;
import com.example.clockifyfinal.model.ActivityListModel;
import com.example.clockifyfinal.service.ClockifyService;
import com.example.clockifyfinal.service.auth.ActivityService;
import com.example.clockifyfinal.util.ConstantManager;
import com.example.clockifyfinal.util.InterfaceManager;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityItemHolder> {
    private ArrayList<ActivityListData> activityListArrayList = new ArrayList<>();
    private Context context;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyy", Locale.getDefault());
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    public ActivityAdapter(Context context) {
        this.context = context;
    }

    public void updateData(ArrayList<ActivityListData> activityArrayList) {
        this.activityListArrayList = activityArrayList;
        notifyDataSetChanged();
    }

    @Override
    public ActivityItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.rv_item, parent, false);
        return new ActivityItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityItemHolder holder, int position) {
        if (activityListArrayList.size() == 0) {

        }else{
            ActivityListData data = activityListArrayList.get(position);
            String startTime = "";
            String endTime = "";
            String dateNext;
            String location = data.location != null ? data.location : "0,0";
            String activity = data.activity != null ? data.activity : "Activity";

            String dateFirst = dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.createdAt));
            if (data.start_time != null) {
                startTime = timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.start_time));
                holder.timeDoing.setText(startTime);
            }
            if (data.end_time != null) {
                endTime = timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.end_time));
            }

            holder.timeRange.setText(startTime + " - " + endTime);
            holder.dateText.setText(dateFirst);
            holder.locationActivity.setText(location);
            holder.activityDescription.setText(activity);

            if (position > 0) {
                dateNext = dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(activityListArrayList.get(position - 1).createdAt));
                if (dateFirst.compareTo(dateNext) == 0) {
                    holder.dateText.setVisibility(View.GONE);
                } else {
                    holder.dateText.setVisibility(View.VISIBLE);
                }
            }
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, ActivityDetailActivity.class);
                intent.putExtra(ConstantManager.ID_ACTIVITY_DETAIL,String.valueOf(data.id));
                context.startActivity(intent);
            });
        }

    }

    @Override
    public int getItemCount() {
        return activityListArrayList.size();
    }

    class ActivityItemHolder extends RecyclerView.ViewHolder {
        TextView dateText;
        TextView activityDescription;
        TextView timeRange;
        TextView timeDoing;
        TextView locationActivity;

        public ActivityItemHolder(@NonNull View itemView) {
            super(itemView);

            dateText = itemView.findViewById(R.id.dateText);
            activityDescription = itemView.findViewById(R.id.activityDescription);
            timeRange = itemView.findViewById(R.id.rangeTime);
            timeDoing = itemView.findViewById(R.id.timeDoing);
            locationActivity = itemView.findViewById(R.id.locationDescription);
        }
    }

}
