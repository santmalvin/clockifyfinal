package com.example.clockifyfinal.activity;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.util.UserDefaults;

public class SplashScreenActivity extends Activity {
    private String token = UserDefaults.getInstance().getString(UserDefaults.TOKEN_KEY);
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        handler = new Handler();
        handler.postDelayed(() -> {
            finish();
            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(intent);
        }, 1000);

    }
}