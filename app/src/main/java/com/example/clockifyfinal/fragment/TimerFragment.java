package com.example.clockifyfinal.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.service.ClockifyService;
import com.example.clockifyfinal.service.auth.ActivityService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimerFragment extends Fragment {
    TextView startTime;
    TextView startDate;
    TextView endTime;
    TextView endDate;
    TextView activityTask;
    Button startStop;
    Button stopTimer;
    Button deleteButton;
    Chronometer chronometer;

    private boolean running = false;
    private long pauseOffset = 0;

    private String startTimeData;
    private String endTimeData;

    private Date currentTime;
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

    public static SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = ((ViewGroup) inflater.inflate(R.layout.fragment_timer, container, false));

        initUI(rootView);
        initLayout();
        initEvent();

        return rootView;
    }

    private void initUI(ViewGroup rootView) {
        startTime = rootView.findViewById(R.id.startTime);
        startDate = rootView.findViewById(R.id.startDate);
        endTime = rootView.findViewById(R.id.endTime);
        endDate = rootView.findViewById(R.id.endDate);
        activityTask = rootView.findViewById(R.id.activityEditText);
        startStop = rootView.findViewById(R.id.startTimerButton);
        stopTimer = rootView.findViewById(R.id.stopTimerButton);
        deleteButton = rootView.findViewById(R.id.deleteButton);
        chronometer = rootView.findViewById(R.id.stopwatchTimer);
    }

    private void initLayout() {
        setEmptyText();
        stopTimer.setVisibility(View.GONE);
        deleteButton.setVisibility(View.GONE);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.setText("00:00:00");
    }

    private void setEmptyText() {
        startTime.setText("-");
        endTime.setText("-");
        startDate.setText("-");
        endDate.setText("-");
    }

    private void initEvent() {
        chronometer.setOnChronometerTickListener(chronometer1 -> {
            long time = SystemClock.elapsedRealtime() - chronometer1.getBase();
            int h = (int) (time / 3600000);
            int m = (int) (time - h * 3600000) / 60000;
            int s = (int) (time - h * 3600000 - m * 60000) / 1000;
            String t = (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s);
            chronometer1.setText(t);
        });

        startStop.setOnClickListener(v -> {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            startStop.setText("Stop");

            setTimeDateRecord(startTime, startDate);

            stopTimer.setVisibility(View.VISIBLE);
            deleteButton.setVisibility(View.VISIBLE);
            startStop.setVisibility(View.GONE);

            running = true;
            startTimeData = timeFormatter.format(currentTime);

        });

        stopTimer.setOnClickListener(v -> {
            if (running) {
                chronometer.stop();
                pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();

                startStop.setText("Start");
                stopTimer.setText("Save");

                setTimeDateRecord(endTime, endDate);
                running = false;
                endTimeData = timeFormatter.format(currentTime);
            } else {
                createActivity();
            }
        });

        deleteButton.setOnClickListener(v -> {
            setEmptyText();
            chronometer.stop();
            chronometer.setBase(SystemClock.elapsedRealtime());
            pauseOffset = 0;
            chronometer.setText("00:00:00");
            stopTimer.setVisibility(View.GONE);
            deleteButton.setVisibility(View.GONE);
            startStop.setVisibility(View.VISIBLE);
            startStop.setText("Start");
        });
    }

    private void setTimeDateRecord(TextView textViewEndTime, TextView textViewEndDate) {
        currentTime = Calendar.getInstance().getTime();
        String output = timeFormat.format(currentTime);
        textViewEndTime.setText(output);

        Date currentDate = Calendar.getInstance().getTime();
        String dateOutput = dateFormat.format(currentDate);
        textViewEndDate.setText(dateOutput);
    }

    private void createActivity() {
        ActivityService service = ClockifyService.create(ActivityService.class);
        service.createActivtiyList(startTimeData, endTimeData, startDate.getText().toString(), endDate.getText().toString(),
                "", activityTask.getText().toString(), "").enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), "Create Activity Success", Toast.LENGTH_SHORT).show();
                    setEmptyText();
                    chronometer.stop();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    pauseOffset = 0;
                    chronometer.setText("00:00:00");
                    stopTimer.setVisibility(View.GONE);
                    deleteButton.setVisibility(View.GONE);
                    startStop.setVisibility(View.VISIBLE);
                    startStop.setText("Start");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}