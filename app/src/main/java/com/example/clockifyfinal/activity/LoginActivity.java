package com.example.clockifyfinal.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.clockifyfinal.R;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {

    private Button createButton;
    private Button signinButton;
    private TextInputEditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUI();
        initEvent();
    }

    private void initUI() {
        createButton = findViewById(R.id.tv_create_account);
        signinButton = findViewById(R.id.signInButton);
        email = findViewById(R.id.text_input_edit_email);

    }

    private void initEvent() {
        createButton.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        });

        signinButton.setOnClickListener(v -> {
            final String Email = email.getText().toString();
            if (Email.length() == 0) {
                email.requestFocus();
                email.setError("FIELD CANNOT BE EMPTY");
            } else {
                String emailString = email.getText().toString();
                Intent intent = new Intent(LoginActivity.this, PasswordLoginActivity.class);
                intent.putExtra("email_string", emailString);
                startActivity(intent);
            }
        });
    }
}