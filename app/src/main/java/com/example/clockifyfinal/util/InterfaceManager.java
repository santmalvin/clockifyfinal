package com.example.clockifyfinal.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class InterfaceManager {
    private static InterfaceManager interfaceManager = null;
    private SimpleDateFormat parser;

    public static InterfaceManager getInstance() {
        if (interfaceManager == null) {
            interfaceManager = new InterfaceManager();
        }
        return interfaceManager;
    }

    public Date stringDateFormatter(String dateString) {
        Date date = null;
        if (parser == null) {
            parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        }
        parser.setTimeZone(TimeZone.getTimeZone("GMT+0:00"));
        try {
            date = parser.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }
}
