package com.example.clockifyfinal.model;

public class ActivityListData {
    public Integer id;
    public Integer user_id;
    public String password;
    public String start_time;
    public String end_time;
    public String activity;
    public String location;
    public String createdAt;
    public String updatedAt;
    public String deletedAt;
}
