package com.example.clockifyfinal.fragment;

import android.os.Bundle;

import androidx.annotation.AnyRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.adapter.ActivityAdapter;
import com.example.clockifyfinal.model.ActivityList;
import com.example.clockifyfinal.model.ActivityListData;
import com.example.clockifyfinal.model.ActivityListModel;
import com.example.clockifyfinal.service.ClockifyService;
import com.example.clockifyfinal.service.auth.ActivityService;

import java.lang.reflect.Array;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityListFragment extends Fragment {

    private RecyclerView activityList;

    private ArrayList<ActivityListData> activityListArrayList = new ArrayList<>();

    private ActivityAdapter activityAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = ((ViewGroup) inflater.inflate(R.layout.fragment_activity_list, container, false));

        activityList = (RecyclerView) rootView.findViewById(R.id.activityList);

        initLayout();
        getActivityList();

        return rootView;
    }

    public void initLayout() {
        activityAdapter = new ActivityAdapter(getContext());
        activityList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        activityList.setAdapter(activityAdapter);
    }

    public void getActivityList() {

        ActivityService activityService = ClockifyService.create(ActivityService.class);
        activityService.getActivityList().enqueue(new Callback<ActivityListModel>() {
            @Override
            public void onResponse(Call<ActivityListModel> call, Response<ActivityListModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    activityListArrayList = response.body().results;
                    activityAdapter.updateData(activityListArrayList);
                } else {

                }
            }

            @Override
            public void onFailure(Call<ActivityListModel> call, Throwable t) {

            }
        });
    }
}