package com.example.clockifyfinal.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clockifyfinal.R;
import com.example.clockifyfinal.fragment.ActivityListFragment;
import com.example.clockifyfinal.model.ActivityDetailModel;
import com.example.clockifyfinal.model.ActivityListData;
import com.example.clockifyfinal.service.ClockifyService;
import com.example.clockifyfinal.service.auth.ActivityService;
import com.example.clockifyfinal.ui.ToolBarBackWhiteView;
import com.example.clockifyfinal.util.ConstantManager;
import com.example.clockifyfinal.util.InterfaceManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDetailActivity extends AppCompatActivity {
    private TextView dateNow;
    private TextView totalDuration;
    private EditText activityDescription;
    private TextView startTime;
    private TextView endTime;
    private TextView startDate;
    private TextView endDate;
    private Button delete;
    private Button save;
    private ToolBarBackWhiteView back;
    private ActivityListData data = new ActivityListData();
    private String id;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyy", Locale.getDefault());
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getActivityDetail();
    }


    private void initUI() {
        dateNow = findViewById(R.id.dateDetail);
        totalDuration = findViewById(R.id.stopwatchTimer);
        activityDescription = findViewById(R.id.activityDetail);
        startTime = findViewById(R.id.startTimeDetail);
        endTime = findViewById(R.id.endTimeDetail);
        startDate = findViewById(R.id.startDateDetail);
        endDate = findViewById(R.id.endDateDetail);
        back = findViewById(R.id.back_Button_detail);
        delete = findViewById(R.id.deleteButtonDetail);
        save = findViewById(R.id.saveButtonDetail);
        String dateFirst = dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.createdAt));
        dateNow.setText(dateFirst);
        if(data.start_time != null) {
            startTime.setText(timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.start_time)));
            startDate.setText(dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.start_time)));
        }else{
            startTime.setText("-");
            startDate.setText("-");
        }
        if (data.end_time != null) {
            endTime.setText(timeFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.end_time)));
            endDate.setText(dateFormat.format(InterfaceManager.getInstance().stringDateFormatter(data.end_time)));
        }else{
            endTime.setText("-");
            endDate.setText("-");
        }
        activityDescription.setText(data.activity);
        initEvent();

    }

    private void initEvent(){
        back.setOnBackClickListener(v ->{
            Intent intent = new Intent(ActivityDetailActivity.this, HomeActivity.class);
            startActivity(intent);
        });
        delete.setOnClickListener(v -> {
            deleteActivity();
        });
        save.setOnClickListener(v -> {
            updateActivity();
        });

    }
    public void initData(){
        id = getIntent().getStringExtra(ConstantManager.ID_ACTIVITY_DETAIL);
    }
    public void getActivityDetail() {
        initData();
        ActivityService activityService = ClockifyService.create(ActivityService.class);
        activityService.getActivityDetail(id).enqueue(new Callback<ActivityDetailModel>() {
            @Override
            public void onResponse(Call<ActivityDetailModel> call, Response<ActivityDetailModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    data = response.body().results;
                    initUI();
                } else {

                }
            }

            @Override
            public void onFailure(Call<ActivityDetailModel> call, Throwable t) {

            }
        });
    }

    public void deleteActivity(){
        ActivityService activityService = ClockifyService.create(ActivityService.class);
        activityService.deleteActivity(id).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(ActivityDetailActivity.this,"Delete Success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ActivityDetailActivity.this, HomeActivity.class);
                    startActivity(intent);
                } else {

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    public void updateActivity(){
        ActivityService activityService = ClockifyService.create(ActivityService.class);
        activityService.updateActivity(id,activityDescription.getText().toString()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(ActivityDetailActivity.this,"Update Success", Toast.LENGTH_SHORT).show();
                } else {

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}